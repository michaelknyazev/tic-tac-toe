(() => {
  /* Start Helpers Section */
  // Reset field to it's original state
  const resetPlayField = () => ([0,1,2,3,4,5,6,7,8].map(position => ({ position, state: -1 })));

  // Switch player. Work only with 2 players with playerId === 0 || 1;
  const switchPlayer = (current) => current === 0 ? 1 : 0;
  
  // Check winner function. Accepts current field and moveId.
  // This is a mock function for now, and need's to be created.
  const checkWinnerGuy = (field, moveId) => {
    // For now, we will finish the game every maxmove value, which is 9.
    if (moveId === 9) return -1;

    // Define current matrix.
    const _defineCurrentMatrix = field.map(_s => _s.state);

    console.log(_defineCurrentMatrix);
    /*

    Here is possible winner matrix's

    // Horizontal wins
    [0,0,0,"","","","","",""]; // First row
    ["","","",0,0,0,"","",""]; // Second row
    ["","","","","","",0,0,0]; // Third row
    // Vertical wins
    [0,"","",0,"","",0,"",""]; // First col
    ["",0,"","",0,"",0,"",""] // Second col
    ["","",0,"","",0,"","",0] // Third col
    // Diagonal Wins
    [0,"","","",0,"","","",0]; // Left to right
    ["","",0,"",0,"",0,"",""] // Right to left
    */
  };
  /* End Helpers Section */

  /* Start State / Data Section */
  const boot = document.getElementById('boot');
  // Array Of Available Players
  const _players = [
    { name: 'The O Guy', id: 0, symbol: "O" },
    { name: 'The X Guy', id: 1, symbol: "X" }
  ];
  // Define active Player
  let _activePlayerId = 0;
  // Define PlayField
  let _playField = resetPlayField();
  // Define Move Counter
  let _moveId = 0;
  /* End State / Data Section */

  /* Start Handlers / Work with Data Section */
  // Move Click Handler
  const handleMove = (position) => {
    _playField = _playField.map(_section => {
      if (_section.position === position) {
        _section.state = _activePlayerId;
      }

      return _section;
    });

    _activePlayerId = switchPlayer(_activePlayerId);

    _moveId++;

    draw(checkWinnerGuy(_playField, _moveId));
  };

  // Restart Game Click handler
  const handleRestartGame = () => {
    _playField = resetPlayField();
    _activePlayerId = 0;
    _moveId = 0;

    draw();
  }
  /* End Handlers Section */

  /* Start Render Section */
  const draw = (winnerId) => {
    console.log(`Draw for move ${_moveId} started at ${Date.now()}`);

    const _playFieldContainer = document.createElement('div');
    _playFieldContainer.setAttribute('id', 'play-field-container');
    _playFieldContainer.classList.add('play-field');

    _playField.map(_section => {
      const _sectionContainer = document.createElement('span');
            _sectionContainer.classList.add('play-field__section');
      
      if (_section.state === -1 && winnerId === undefined) {
        _sectionContainer.addEventListener('click', () => handleMove(_section.position))              
      }

      let _sectionContent = "";

      if (_section.state !== -1) {
        const { symbol } = _players[_section.state];

        _sectionContent = symbol;
      }

      _sectionContainer.innerHTML = _sectionContent;
      _playFieldContainer.appendChild(_sectionContainer);
    });

    const isChildExists = document.getElementById('play-field-container');

    if (isChildExists) {
      const parent = isChildExists.parentNode;
      parent.replaceChild(_playFieldContainer, isChildExists);
    } else {
      boot.appendChild(_playFieldContainer);
    }

    if (winnerId !== undefined) {
      const _winnerData = _players.find(_p => _p.id === winnerId);
      const _winnerContainer = document.createElement('div');
            _winnerContainer.classList.add('winner-container');

      if (_winnerData) {
        _winnerContainer.innerHTML = `${_winnerData.name} won!`;
      } else {
        _winnerContainer.innerHTML = `Stalemate!`;
      }
        
      const _resetButton = document.createElement('button');
            _resetButton.classList.add('button-reset')
            _resetButton.addEventListener('click', handleRestartGame);
            _resetButton.innerHTML = 'Click to restart';
      
      _winnerContainer.appendChild(_resetButton);
      _playFieldContainer.appendChild(_winnerContainer);
    }

    console.log(`Draw for move ${_moveId} ended at ${Date.now()}`);
  }
  /* End Render Section */

  // Launch app
  draw();
})();